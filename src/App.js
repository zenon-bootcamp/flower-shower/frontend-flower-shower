import { useState } from 'react'
import { BrowserRouter } from 'react-router-dom'
import AuthContext from './contexts/auth'
import CartContext from './contexts/cart'
import Router from './Router'
import './global.css'

export default function App() {
    let authTokenOnStorage = localStorage.getItem('flower-shower-auth-token')
    authTokenOnStorage = authTokenOnStorage === 'null' ? null : authTokenOnStorage
    const [authToken, setAuthToken] = useState(authTokenOnStorage ?? null)
    const [cart, setCart] = useState([])
    const setAuthTokenWithStorage = (authToken) => {
        localStorage.setItem('flower-shower-auth-token', authToken)
        setAuthToken(authToken)
    }
    return (
        <AuthContext.Provider value={{ authToken, setAuthToken: setAuthTokenWithStorage }}>
            <CartContext.Provider value={{ cart, setCart }}>
                <BrowserRouter>
                    <Router />
                </BrowserRouter>
            </CartContext.Provider>
        </AuthContext.Provider>
    )
}