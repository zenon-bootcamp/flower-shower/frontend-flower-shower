import { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import AuthContext from '../../contexts/auth'
import './style.css'

export default function ProfilePage() {
    const { authToken } = useContext(AuthContext)
    const history = useHistory()
    const [name, setName] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    useEffect(() => {
        if (authToken) {
            fetch('https://flower-shower-camt-bootcamp.pair-co.com/user', {
                headers: {
                    'Authorization': `Bearer ${authToken}`
                }
            })
                .then(response => response.json())
                .then(data => {
                    setName(data.name)
                    setPhoneNumber(data.phone_number)
                })
        }
    }, [])

    const edit = (event) => {
        event.preventDefault()
        if (!name) {
            alert('กรุณากรอก ชื่อ-นามสกุล')
        }
        else if (!phoneNumber) {
            alert('กรุณากรอก เบอร์โทรศัพท์')
        }
        else {
            fetch('https://flower-shower-camt-bootcamp.pair-co.com/user', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${authToken}`
                },
                body: JSON.stringify({
                    name,
                    phone_number: phoneNumber,
                })
            })
                .then(response => response.json())
                .then(() => {
                    alert('แก้ไขข้อมูลเรียบร้อยแล้ว')
                })
        }
    }

    if (!authToken) {
        history.replace('/login')
        return null
    }

    return (
        <div className="profile">
            <form onSubmit={edit}>
                <div>
                    <label>ชื่อ - สกุล :</label>
                    <input type="text" value={name} onChange={event => setName(event.target.value)} />
                </div>
                <div>
                    <label>เบอร์โทรศัพท์ :</label>
                    <input type="text" value={phoneNumber} onChange={event => setPhoneNumber(event.target.value)} />
                </div>
                <div className="profile-submit-button">
                    <button>แก้ไข</button>
                </div>
            </form>
        </div>
    )
}