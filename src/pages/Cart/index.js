import { useContext } from 'react'
import { useHistory } from 'react-router'
import CartContext from '../../contexts/cart'
import AuthContext from '../../contexts/auth'
import './style.css'

export default function CartPage() {
    const { cart, setCart } = useContext(CartContext)
    const { authToken } = useContext(AuthContext)
    const history = useHistory()

    const removeBouquet = (targetIndex) => {
        setCart(cart.filter((bouquet, index) => index !== targetIndex))
    }

    const order = () => {
        if (authToken) {
            fetch('https://flower-shower-camt-bootcamp.pair-co.com/order', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${authToken}`
                },
                body: JSON.stringify({ bouquets_id: cart.map(bouquet => bouquet.id) })
            })
                .then(response => response.json())
                .then(data => {
                    alert('ขอบคุณสำหรับคำสั่งซื้อ สินค้าของท่านกำลังถูกจัดเตรียม กรุณารอเราติดต่อกลับไป')
                    setCart([])
                    history.push('/order/history')
                })
        }
        else {
            alert('กรุณา เข้าสู่ระบบ ก่อนสั่งสินค้า')
            history.push('/login')
        }
    }

    const total = cart.reduce((sum, bouquet) => sum += bouquet.price, 0)

    return (
        <div className="cart-list-container">
            <div className="cart-header">Your Cart ({cart.length})</div>
            <hr />
            <div className="cart-list">
                {cart.map((bouquet, index) => (
                    <div key={index} className="cart-bouquet">
                        <div className="cart-bouquet-left">
                            <div className="cart-bouquet-image">
                                <img src={bouquet.image} alt={bouquet.name} />
                            </div>
                            <div className="cart-bouquet-data">
                                <div className="cart-bouquet-name">{bouquet.name}</div>
                                <div className="cart-bouquet-flowers">{bouquet.flowers}</div>
                            </div>
                        </div>
                        <div className="cart-bouquet-right">
                            <div className="cart-bouquet-price">{bouquet.price} บาท</div>
                            <div className="cart-bouquet-remove">
                                <button onClick={() => removeBouquet(index)}>X</button>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <hr />
            <div className="cart-total">
                รวมเป็นเงิน <span>{total}</span> บาท
            </div>
            <div className="cart-confirm">
                <button onClick={order} disabled={cart.length === 0}>ยืนยันการสั่ง</button>
            </div>
        </div>
    )
}