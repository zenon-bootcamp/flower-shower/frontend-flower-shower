import { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import AuthContext from '../../contexts/auth'
import './style.css'

export default function OrderHisotryPage() {
    const { authToken } = useContext(AuthContext)
    const history = useHistory()
    const [ orders, setOrders ] = useState([])

    useEffect(() => {
        if (authToken) {
            fetch('https://flower-shower-camt-bootcamp.pair-co.com/order', {
                headers: {
                    'Authorization': `Bearer ${authToken}`
                }
            })
                .then(response => response.json())
                .then(data => setOrders(data.reverse()))
        }
    }, [])

    if (!authToken) {
        history.replace('/login')
        return null
    }

    return orders.map(order => (
        <div key={order.id} className="order-list-container">
            <div className="order-header">Order #{order.id}</div>
            <div className="order-subHeader">{new Date(order.updated_at).toDateString()}</div>
            <hr />
            <div className="order-list">
                {order.bouquets.map((bouquet, index) => (
                    <div key={index} className="order-bouquet">
                        <div className="order-bouquet-left">
                            <div className="order-bouquet-image">
                                <img src={bouquet.image} alt={bouquet.name} />
                            </div>
                            <div className="order-bouquet-data">
                                <div className="order-bouquet-name">{bouquet.name}</div>
                                <div className="order-bouquet-flowers">{bouquet.flowers}</div>
                            </div>
                        </div>
                        <div className="order-bouquet-right">
                            <div className="order-bouquet-price">{bouquet.price} บาท</div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    ))
}