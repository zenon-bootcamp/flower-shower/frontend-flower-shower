import { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import CartContext from '../../contexts/cart'
import './style.css'

export default function BouquetListPage() {
    const [bouquets, setBouquets] = useState([])
    useEffect(() => {
        fetch('https://flower-shower-camt-bootcamp.pair-co.com/bouquet')
            .then(response => response.json())
            .then(bouquets => setBouquets(bouquets))
    }, [])

    const { cart, setCart } = useContext(CartContext)
    const history = useHistory()

    const addToCart = (bouquet, event) => {
        event.stopPropagation()
        setCart([...cart, bouquet])
        history.push('/cart')
    }

    return (
        <div className="bouquet-list">
            {bouquets.map(bouquet => (
                <div key={bouquet.id} className="bouquet" onClick={() => history.push(`/bouquet/${bouquet.id}`)}>
                    <img src={bouquet.image} alt={bouquet.name} />
                    <div className="bouquet-data">
                        <div className="bouquet-name">{bouquet.name}</div>
                        <div className="bouquet-flowers">{bouquet.flowers}</div>
                        <div className="bouquet-bottom">
                            <div className="bouquet-price">{bouquet.price} บาท</div>
                            <div className="bouquet-add-to-cart">
                                <button onClick={(event) => addToCart(bouquet, event)}>+</button>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}