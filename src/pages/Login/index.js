import { useContext, useState } from 'react'
import { useHistory } from 'react-router'
import AuthContext from '../../contexts/auth'
import './style.css'

export default function LoginPage() {
    const { setAuthToken } = useContext(AuthContext)
    const history = useHistory()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const login = (event) => {
        event.preventDefault()
        if (email && password) {
            fetch('https://flower-shower-camt-bootcamp.pair-co.com/user/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email, password })
            })
                .then(response => response.json())
                .then(data => {
                    if (data.errors) {
                        alert('Email หรือ Password ผิด กรุณาตรวจสอบใหม่')
                    }
                    else {
                        setAuthToken(data.token)
                        history.push('/')
                    }
                })
        }
        else {
            alert('กรุณากรอก Email และ Password ให้ครบถ้วน')
        }
    }
    return (
        <div className="login">
            <form onSubmit={login}>
                <div>
                    <label>E-Mail :</label>
                    <input type="email" value={email} onChange={event => setEmail(event.target.value)} />
                </div>
                <div>
                    <label>Password:</label>
                    <input type="password" value={password} onChange={event => setPassword(event.target.value)} />
                </div>
                <div className="login-submit-button">
                    <button>เข้าสู่ระบบ</button>
                </div>
            </form>
        </div>
    )
}