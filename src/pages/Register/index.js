import { useState } from 'react'
import { useHistory } from 'react-router'
import './style.css'

export default function RegisterPage() {
    const history = useHistory()
    const [name, setName] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [passwordConfirmation, setPasswordConfirmation] = useState('')
    const register = (event) => {
        event.preventDefault()
        if (!name) {
            alert('กรุณากรอก ชื่อ-นามสกุล')
        }
        else if (!phoneNumber) {
            alert('กรุณากรอก เบอร์โทรศัพท์')
        }
        else if (!email) {
            alert('กรุณากรอก Email')
        }
        else if (!password) {
            alert('กรุณากรอก Password')
        }
        else if (!passwordConfirmation) {
            alert('กรุณากรอก Password อีกครั้ง')
        }
        else if (password !== passwordConfirmation) {
            alert('Password ทั้งสองช่อง ไม่เหมือนกัน')
        }
        else {
            fetch('https://flower-shower-camt-bootcamp.pair-co.com/user', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name,
                    phone_number: phoneNumber,
                    email,
                    password,
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data.email && Array.isArray(data.email) && data.email.includes('has already been taken')) {
                        alert('Email ถูกใช้แล้ว กรุณาใช้อันอื่น')
                    }
                    else {
                        alert('สมัครสมาชิกเรียบร้อยแล้ว กรุณา Login')
                        history.push('/login')
                    }
                })
        }
    }
    return (
        <div className="register">
            <form onSubmit={register}>
                <div>
                    <label>ชื่อ - สกุล :</label>
                    <input type="text" value={name} onChange={event => setName(event.target.value)} />
                </div>
                <div>
                    <label>เบอร์โทรศัพท์ :</label>
                    <input type="text" value={phoneNumber} onChange={event => setPhoneNumber(event.target.value)} />
                </div>
                <div>
                    <label>E-Mail :</label>
                    <input type="email" value={email} onChange={event => setEmail(event.target.value)} />
                </div>
                <div>
                    <label>Password:</label>
                    <input type="password" value={password} onChange={event => setPassword(event.target.value)} />
                </div>
                <div>
                    <label>Password อีกครั้ง:</label>
                    <input type="password" value={passwordConfirmation} onChange={event => setPasswordConfirmation(event.target.value)} />
                </div>
                <div className="register-submit-button">
                    <button>สมัครสมาชิก</button>
                </div>
            </form>
        </div>
    )
}