import { useContext, useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router'
import CartContext from '../../contexts/cart'
import './style.css'

export default function BouquetDetailPage() {
    const [bouquet, setBouquet] = useState(null)
    const { cart, setCart } = useContext(CartContext)
    const history = useHistory()
    const params = useParams()

    useEffect(() => {
        fetch(`https://flower-shower-camt-bootcamp.pair-co.com/bouquet/${params.id}`)
            .then(response => response.json())
            .then(data => setBouquet(data))
            .catch(() => {
                history.replace('/bouquet/list')
            })
    }, [])

    const addToCart = () => {
        setCart([...cart, bouquet])
        history.push('/cart')
    }

    if (!bouquet) {
        return null
    }

    return (
        <div className="detail">
            <div className="detail-left">
                <img src={bouquet.image} alt={bouquet.name} />
            </div>
            <div className="detail-right">
                <div className="detail-bouquet-name">{bouquet.name}</div>
                <div className="detail-bouquet-flowers">{bouquet.flowers}</div>
                <div className="detail-bouquet-price">{bouquet.price} บาท</div>
                <div className="detail-bouquet-descrption">{bouquet.description}</div>
                <div className="detail-bouquet-cart">
                    <button onClick={addToCart}>
                        หยิบใส่ตระกร้า
                    </button>
                </div>
            </div>
        </div>
    )
}