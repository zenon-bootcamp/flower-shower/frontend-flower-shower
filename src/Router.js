import { Route, Switch } from 'react-router-dom'
import Navbar from './components/Navbar'
import BouquetDetailPage from './pages/BouquetDetail'
import BouquetListPage from './pages/BouquetList'
import CartPage from './pages/Cart'
import ProfilePage from './pages/Profile'
import LandingPage from './pages/Landing'
import LoginPage from './pages/Login'
import OrderHistoryPage from './pages/OrderHistory'
import RegisterPage from './pages/Register'

export default function Router() {
    return (
        <>
            <Navbar />
            <Switch>
                <Route exact path="/bouquet/list" component={BouquetListPage} />
                <Route exact path="/bouquet/:id" component={BouquetDetailPage} />
                <Route exact path="/cart" component={CartPage} />
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/register" component={RegisterPage} />
                <Route exact path="/profile" component={ProfilePage} />
                <Route exact path="/order/history" component={OrderHistoryPage} />
                <Route exact path="/" component={LandingPage} />
            </Switch>
        </>
    )
}