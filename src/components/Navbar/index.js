import { useContext } from 'react'
import { Link, useHistory } from 'react-router-dom'
import CartContext from '../../contexts/cart'
import AuthContext from '../../contexts/auth'
import './style.css'

export default function Navbar() {
    const { cart } = useContext(CartContext)
    const { authToken, setAuthToken } = useContext(AuthContext)
    const history = useHistory()
    const logout = () => {
        setAuthToken(null)
        history.push('/login')
    }
    return (
        <nav className="nav">
            <section className="nav-menu nav-left">
                <ul>
                    <li>
                        <img className="nav-logo" src={require('../../assets/logo.png').default} alt="Flower Shower" />
                    </li>
                    <li>
                        <Link to="/">
                            หน้าแรก
                        </Link>
                    </li>
                    <li>
                        <Link to="/bouquet/list">
                            สินค้า
                        </Link>
                    </li>
                </ul>
            </section>
            <section className="nav-menu nav-right">
                <ul>
                    <li className="nav-cart">
                        <Link to="/cart">
                            ตระกร้า <span className="nav-cart-amount">{cart.length}</span>
                        </Link>
                    </li>
                    {
                        authToken ?
                        (
                            <>
                                <li>
                                    <Link to="/profile">
                                        ข้อมูลส่วนตัว
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/order/history">
                                        ประวัติคำสั่งซื้อ
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/login" onClick={logout}>
                                        ออกจากระบบ
                                    </Link>
                                </li>
                            </>
                        )
                        :
                        (
                            <>
                                <li>
                                    <Link to="/login">
                                        เข้าสู่ระบบ
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/register">
                                        สมัครสมาชิก
                                    </Link>
                                </li>
                            </>
                        )
                    }
                </ul>
            </section>
        </nav>
    )
}